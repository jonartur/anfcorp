package com.example.anfcorp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnfcorpApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnfcorpApplication.class, args);
	}

}
